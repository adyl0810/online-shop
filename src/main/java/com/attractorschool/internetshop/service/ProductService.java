package com.attractorschool.internetshop.service;


import com.attractorschool.internetshop.model.*;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class ProductService {
    private final ProductRepository _productRepository;
    private final CategoryRepository categoryRepository;
    private final ManufacturerService manufacturerService;
    private final CategoryService categoryService;
    private final ManufacturerRepository manufacturerRepository;

    public ProductService(ProductRepository _productRepository, CategoryRepository categoryRepository, ManufacturerService manufacturerService, CategoryService categoryService, ManufacturerRepository manufacturerRepository) {
        this._productRepository = _productRepository;
        this.categoryRepository = categoryRepository;
        this.manufacturerService = manufacturerService;
        this.categoryService = categoryService;
        this.manufacturerRepository = manufacturerRepository;
    }

    public Product firstOrDefault(String name) {
        return _productRepository.findByName(name);
    }

    public Product getById(String id) {
        return _productRepository.findById(id).orElseThrow(() ->
                new ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        String.format("Продукт с id %s не найден", id)
                ));
    }

    public void addProduct(String name, int price, String description, String manufacturerName, String categoryName) {
        Product product = new Product(name, price, description);
        Manufacturer manufacturer = manufacturerRepository.findByName(manufacturerName);
        Category category = categoryRepository.findByName(categoryName);
        product.setManufacturer(manufacturer);
        product.setCategory(category);
        _productRepository.save(product);
    }

    public void editProduct(Product product, String name, Integer price, String description) {
        if (name != null)
            product.setName(name);
        if (price != null)
            product.setPrice(price);
        if (description != null)
            product.setDescription(description);
    }

}
