package com.attractorschool.internetshop.service;


import com.attractorschool.internetshop.model.Category;
import com.attractorschool.internetshop.model.CategoryRepository;
import com.attractorschool.internetshop.model.Manufacturer;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class CategoryService {
    private final CategoryRepository _categoryRepository;

    public CategoryService(CategoryRepository _categoryRepository) {
        this._categoryRepository = _categoryRepository;
    }

    public Category firstOrDefault(String name) {
        return _categoryRepository.findByName(name);
    }

    public Category getById(String id) {
        return _categoryRepository.findById(id).orElseThrow(() ->
                new ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        String.format("Категория с id %s не найден", id)
                ));
    }

    public void editCategory(Category category, String name) {
        if (name != null)
            category.setName(name);
    }
    public Category addCategory(String name) {
        return new Category(name);
    }

}
