package com.attractorschool.internetshop.service;

import com.attractorschool.internetshop.model.Manufacturer;
import com.attractorschool.internetshop.model.ManufacturerRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class ManufacturerService {
    private final ManufacturerRepository _manufacturerRepository;

    public ManufacturerService(ManufacturerRepository _manufacturerRepository) {
        this._manufacturerRepository = _manufacturerRepository;
    }

    public Manufacturer firstOrDefault(String name) {
        return _manufacturerRepository.findByName(name);
    }

    public Manufacturer getById(String id) {
        return _manufacturerRepository.findById(id).orElseThrow(() ->
                new ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        String.format("Производитель с id %s не найден", id)
                ));
    }

    public Manufacturer addManufacturer(String name) {
        return new Manufacturer(name);
    }

    public void editManufacturer(Manufacturer manufacturer, String name) {
        if (name != null)
            manufacturer.setName(name);
    }
}
