package com.attractorschool.internetshop.model;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface ManufacturerRepository extends MongoRepository<Manufacturer, String> {
    Manufacturer findByName(String name);
}
