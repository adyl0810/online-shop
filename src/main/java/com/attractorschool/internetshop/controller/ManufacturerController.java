package com.attractorschool.internetshop.controller;

import com.attractorschool.internetshop.model.Manufacturer;
import com.attractorschool.internetshop.model.ManufacturerRepository;
import com.attractorschool.internetshop.model.Product;
import com.attractorschool.internetshop.service.ManufacturerService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Controller
public class ManufacturerController {
    private final ManufacturerRepository _manufacturerRepository;
    private final ManufacturerService manufacturerService;

    public ManufacturerController(ManufacturerRepository _manufacturerRepository, ManufacturerService manufacturerService) {
        this._manufacturerRepository = _manufacturerRepository;
        this.manufacturerService = manufacturerService;
    }

    @GetMapping("/manufacturers")
    public String getManufacturers(Model model) {
        List<Manufacturer> manufacturers = _manufacturerRepository.findAll();
        model.addAttribute("manufacturers", manufacturers);
        return "manufacturers";
    }

    @GetMapping("/manufacturers/{id}")
    public String getManufacturer(Model model,
                             @PathVariable String id) {
        Manufacturer manufacturer = _manufacturerRepository.findById(id).orElse(null);
        if(manufacturer == null) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    String.format("Производитель с id %s не найден", id)
            );
        }
        model.addAttribute("manufacturer", manufacturer);
        return "manufacturer";
    }

    @GetMapping("/manufacturers/{id}/delete")
    public String getManufacturerDelete(Model model,
                                   @PathVariable String id) {
        Manufacturer manufacturer = _manufacturerRepository.findById(id).orElse(null);
        if(manufacturer == null) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    String.format("Производитель с id %s не найден", id)
            );
        }
        model.addAttribute("manufacturer", manufacturer);
        return "delete-manufacturer";
    }

    @PostMapping("/manufacturers")
    public String addManufacturer(@RequestParam String name) {
        Manufacturer manufacturer = new Manufacturer(name);
        _manufacturerRepository.save(manufacturer);
        return "redirect:/manufacturers";
    }

    @PostMapping("/manufacturers/{id}")
    public String editManufacturer(@PathVariable String id,
                              @RequestParam String name) {
        Manufacturer manufacturer = manufacturerService.getById(id);
        manufacturerService.editManufacturer(manufacturer, name);
        _manufacturerRepository.save(manufacturer);
        return "redirect:/manufacturers";
    }
    @PostMapping("/manufacturers/{id}/delete")
    public String deleteManufacturer(@PathVariable String id) {
        Manufacturer manufacturer = manufacturerService.getById(id);
        _manufacturerRepository.delete(manufacturer);
        return "redirect:/manufacturers";
    }
}
