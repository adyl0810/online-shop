package com.attractorschool.internetshop.controller;

import com.attractorschool.internetshop.model.Category;
import com.attractorschool.internetshop.model.CategoryRepository;
import com.attractorschool.internetshop.service.CategoryService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Controller
public class CategoryController {
    private final CategoryRepository _categoryRepository;
    private final CategoryService categoryService;

    public CategoryController(CategoryRepository _categoryRepository, CategoryService categoryService) {
        this._categoryRepository = _categoryRepository;
        this.categoryService = categoryService;
    }

    @GetMapping("/categories")
    public String getCategories(Model model) {
        List<Category> categories = _categoryRepository.findAll();
        model.addAttribute("categories", categories);
        return "categories";
    }

    @GetMapping("/categories/{id}")
    public String getCategory(Model model,
                             @PathVariable String id) {
        Category category = _categoryRepository.findById(id).orElse(null);
        if(category == null) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    String.format("Категория с id %s не найден", id)
            );
        }
        model.addAttribute("category", category);
        return "category";
    }

    @GetMapping("/categories/{id}/delete")
    public String getCategoryDelete(Model model,
                                   @PathVariable String id) {
        Category category = _categoryRepository.findById(id).orElse(null);
        if(category == null) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    String.format("Категория с id %s не найден", id)
            );
        }
        model.addAttribute("category", category);
        return "delete-category";
    }

    @PostMapping("/categories")
    public String addCategory(@RequestParam String name) {
        Category category = new Category(name);
        _categoryRepository.save(category);
        return "redirect:/categories";
    }

    @PostMapping("/categories/{id}")
    public String editCategory(@PathVariable String id,
                              @RequestParam String name) {
        Category category = categoryService.getById(id);
        categoryService.editCategory(category, name);
        _categoryRepository.save(category);
        return "redirect:/categories";
    }
    @PostMapping("/categories/{id}/delete")
    public String deleteCategory(@PathVariable String id) {
        Category category = categoryService.getById(id);
        _categoryRepository.delete(category);
        return "redirect:/categories";
    }
}
