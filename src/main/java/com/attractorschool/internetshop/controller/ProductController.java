package com.attractorschool.internetshop.controller;

import com.attractorschool.internetshop.model.*;
import com.attractorschool.internetshop.service.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.server.ResponseStatusException;
import java.util.List;

@Controller
public class ProductController {

    private final ProductRepository _productRepository;
    private final ManufacturerRepository _manufacturerRepository;
    private final CategoryRepository _categoryRepository;
    private final ProductService productService;

    public ProductController(ProductRepository _productRepository, ManufacturerRepository _manufacturerRepository, CategoryRepository _categoryRepository, ProductService productService) {
        this._productRepository = _productRepository;
        this._manufacturerRepository = _manufacturerRepository;
        this._categoryRepository = _categoryRepository;
        this.productService = productService;
    }

    @GetMapping("/products")
    public String getProducts(Model model) {
        List<Product> products = _productRepository.findAll();
        List<Manufacturer> manufacturers = _manufacturerRepository.findAll();
        List<Category> categories = _categoryRepository.findAll();
        model.addAttribute("products", products);
        model.addAttribute("manufacturers", manufacturers);
        model.addAttribute("categories", categories);
        return "products";
    }

    @GetMapping("/products/{id}")
    public String getProduct(Model model,
                             @PathVariable String id) {
        Product product = _productRepository.findById(id).orElse(null);
        if(product == null) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    String.format("Продукт с id %s не найден", id)
            );
        }
        model.addAttribute("product", product);
        return "product";
    }

    @GetMapping("/products/{id}/delete")
    public String getProductDelete(Model model,
                             @PathVariable String id) {
        Product product = _productRepository.findById(id).orElse(null);
        if(product == null) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    String.format("Продукт с id %s не найден", id)
            );
        }
        model.addAttribute("product", product);
        return "delete-product";
    }

    @PostMapping("/products")
    public String addProduct(@RequestParam String name,
                             @RequestParam int price,
                             @RequestParam String description,
                             @RequestParam String manufacturerName,
                             @RequestParam String categoryName) {
        productService.addProduct(name, price, description, manufacturerName, categoryName);
        return "redirect:/products";
    }

    @PostMapping("/products/{id}")
    public String editProduct(@PathVariable String id,
                              @RequestParam String name,
                              @RequestParam int price,
                              @RequestParam String description) {
        Product product = productService.getById(id);
        productService.editProduct(product, name, price, description);
        _productRepository.save(product);
        return "redirect:/products";
    }
    @PostMapping("/products/{id}/delete")
    public String deleteProduct(@PathVariable String id) {
        Product product = productService.getById(id);
        _productRepository.delete(product);
        return "redirect:/products";
    }


}
